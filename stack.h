/**
 * @file stack.h
 * @author Jannik Vierling 1226434
 * @brief The stack module contains all structures and functions required
 * to implement a basic stack datastructure.
 * @date 8th November 2013
 */

#ifndef DEF_STACK_H
#define DEF_STACK_H

/**
 * @struct stack_t
 * @brief This structure represents a single stack
 */
typedef struct {
	/** @var stack_t::head
	 *  Member 'head' contains a pointer to the first element of the stack */
    struct stack_node *head;

    /** @var stack_t::size
	*  Member 'size' contains the number of elements in this stack */
    int size;
} stack_t;

/**
 * @struct stack_node
 * @brief This structure represents a node of a stack
 */
struct stack_node {
	/** @var stack_node::next
	 *  Member 'next' contains a pointer to the next node or NULL if this
	 *  node is the first */
	struct stack_node *next;

	/** @var stack_node::data
	 *  Member 'data' contains a pointer to the data stored by this node */
	void *data;
};

/**
 * @brief Sets the error handler for the stack module
 *
 * @param ep The error handler that will be used in the stack module
 * @return void
 *
 * @details precondition: ep != NULL
 */
void stack_set_error_handler(void (*ep) (int, const char *, ...));

/** 
 * @brief Initializes the stack s
 *
 * @param s Pointer to a stack 
 * @return void
 * 
 * @details: precondition s != NULL
 */
void init_stack(stack_t *s);

/** 
 * @brief Allocates a new stack node 
 *
 * @return A pointer to the newly allocated stack node 
 */
struct stack_node *new_node(void);

/** 
 * @brief Adds a node on the top of a stack
 *
 * @param s Pointer to the stack
 * @param n Pointer to the node
 * @return void
 *
 * @details: precondition s != NULL && n != NULL
 */
void push(stack_t *s, struct stack_node *n);

/** 
 * @brief Removes the node on the top of the stack and returns a pointer to
 *
 * that node
 * @param s Pointer to the stack
 * @return Pointer to the removed node
 *
 * @details: precondition s != NULL
 */
struct stack_node *pop(stack_t *s);

/** 
 * @brief Returns the node on the top of the stack
 *
 * @param s Pointer to the stack
 * @return Pointer to the element on the top of the stack
 * 
 * @details: precondition s != NULL
 */
struct stack_node *peek(const stack_t *s);

/** 
 * @brief Frees all nodes of the stack
 *
 * @param s Pointer to the stack
 * @return void
 *
 * @details This function also tries to free the memory pointed by the
 * stack_node::data pointers of the stack_node structure
 * @details precondition: s != NULL
 */
void free_stack(stack_t *s);

#endif
