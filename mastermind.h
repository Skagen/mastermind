/**
 * @file mastermind.h
 * @author Jannik Vierling 1226434
 * @brief The mastermind module contains a set of functions that implement
 * Donald Knuth's "Five Guess"-Algorithm adapted to any version of Mastermind
 * @date 8th November 2013
 */

#ifndef DEF_MASTERMIND_H
#define DEF_MASTERMIND_H

#include "stack.h"

/**
 * @brief The number of colors used in the code
 */
#define NUM_COLORS (8)

/**
 * @brief The number of pegs that are used in the code
 */
#define NUM_PEGS (5)

/** @brief The color codes */
enum { beige = 0, darkblue, green, orange, red, black, violet, white };

/**
 * @brief Eliminates from the set of possiblities those for which the score
 * does not match to the score of the current guess
 * @param s The stack with the remaining possiblities
 * @param guess Pointer to an int-Array with exactly NUM_PEGS elements
 * @param red The number of red key-pegs
 * @param white The number of white key-pegs
 * @return void
 *
 * @details precondition: s != NULL
 * @details precondition: red >= 0 && white >= 0
 */
void eliminate_guesses(stack_t *s, const int *guess, int red, int white);

/**
 * Generates the set of the (NUM_COLORS)^(NUM_PEGS) possiblities for the 
 * first guess 
 * @param The stack which saves the set of possiblities
 * @return void
 *
 * @details precondition: s != NULL
 */
void make_5_peg_guesses(stack_t *s);

#endif
