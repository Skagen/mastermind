/**
 * @file stack.c
 * @author Jannik Vierling 1226434
 * @brief The stack module contains all structures and functions required
 * to implement a basic stack datastructure.
 * @date 8th November 2013
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdarg.h>
#include "stack.h"

static void default_error_handler(int r, const char *fmt, ...);

static void (*error_handler) (int, const char*, ...) = default_error_handler;

/**
 * @brief Prints a user defined error message and errno dependend error
 * message to stderr. Terminates the program with return code r
 *
 * @param r The return code
 * @param fmt The format used to print the user defined error message
 * @param ... The arguments for the user defined error message
 * @return void
 *
 * @details if fmt == NULL no user defined error message will be printed
 */
static void default_error_handler(int r, const char *fmt, ...)
{
	va_list args;

	(void) fprintf(stderr, "%s", "stack: ");

	va_start(args, fmt);
	if (fmt != NULL) {
		(void) vfprintf(stderr, fmt, args);
	}
	va_end(args);

	if (errno != 0) {
		(void) fprintf(stderr, " :%s", strerror(errno));
	}
	(void) fprintf(stderr, "\n");

	exit(r);
}

void stack_set_error_handler(void (*eh) (int, const char *, ...))
{
	error_handler = eh;
}

void init_stack(stack_t *s)
{
    s->head = NULL;
    s->size = 0;
}

struct stack_node *new_node(void)
{
    void * n = NULL;

    if ((n = malloc(sizeof(struct stack_node))) == NULL) {
    		error_handler(EXIT_FAILURE, "malloc");
    }

    return (struct stack_node *) n; 
}

void push(stack_t *s, struct stack_node *n)
{
    n->next = s->head;
    s->head = n;
    s->size++;
}

struct stack_node *pop(stack_t *s)
{
    struct stack_node *ret = s->head;

    if (ret != NULL) {
        s->head = ret->next;
        s->size--;
    } else {
        s->head = NULL;
    }

    return ret;
}

struct stack_node *peek(const stack_t *s)
{
    return s->head;
}

void free_stack(stack_t *s)
{
    struct stack_node *n;
    int stack_size;
    int i;

    if (s->head != NULL) {
        stack_size = s->size;
        for (i = 0; i < stack_size; i++) {
            n = pop(s);
            free(n->data);
            free(n);
        }
    }
}
