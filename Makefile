# Mastermind
# Author: Jannik Vierling 1226434
# Date 1.12.2013

CC=gcc
CFLAGS=-std=c99 -Wall -g -pedantic -DENDEBUG -D_BSD_SOURCE -D_XOPEN_SOURCE=500
LDFLAGS=
EXEC=server client

all: $(EXEC)

client: client.c mastermind.o stack.o 
	$(CC) -o $@ $^ $(LDFLAGS)

server: server.o
	$(CC) -o $@ $< $(LDFLAGS)

%.o : %.c
	$(CC) -o $@ -c $< $(CFLAGS)

.PHONY: clean
clean:
	rm -rf *.o
	rm -rf $(EXEC)
