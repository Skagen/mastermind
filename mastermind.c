/**
 * @file mastermind.c
 * @author Jannik Vierling 1226434
 * @brief The mastermind module contains a set of functions that implement
 * Donald Knuth's "Five Guess"-Algorithm adapted to any version of Mastermind
 * @date 8th November 2013
 */

#include <stdio.h>
#include <stdlib.h>

#include "mastermind.h"
#include "stack.h"

/**
 * @brief Calculates the number of fixpoints of the projection from g1 to g2
 *
 * @param g1 Pointer to an int-Array with exactly NUM_PEGS elements 
 * @param g2 Pointer to an int-Array with exactly NUM_PEGS elements
 * @return The number of fixpoints
 */
static int count_fix_points(const int *g1, const int *g2)
{
    int fixPoints = 0;
    int i;

    for (i = 0; i < NUM_PEGS; i++) {
        fixPoints += g1[i] == g2[i] ? 1 : 0;
    }

    return fixPoints;
}

/** 
 * @brief Calculates the number of "permuted" elements in the projection from
 * g1 to g2.
 *
 * @param g1 Pointer to an int-Array with exactly NUM_PEGS elements
 * @param g2 Pointer to an int-Array with exactly NUM_PEGS elements
 * @return The number of permuted elements
 * 
 * @details Permuted means in this case, an element that is not a fixpoint
 * but occurs in both g1 and g2
 */
static int count_permuted_points(const int *g1, const int *g2)
{
    #define FIX (1)
    #define NOT_PERMUTED (0)
    #define PERMUTED (2)

    int mask[NUM_PEGS] = {NOT_PERMUTED};
    int i = 0, j = 0;
    int matched = 0;
    int found = 0;

    /* label the fixpoints */
    for (i = 0; i < NUM_PEGS; i++) {
        if (g1[i] == g2[i]) {
            mask[i] = FIX;
        }
    }

    /* label the permutations */
    for (i = 0; i < NUM_PEGS; i++) {
        found = 0;
        if (mask[i] != FIX) {
            for (j = 0; j < NUM_PEGS && !found; j++) {
                if ((g1[i] == g2[j]) && (mask[j] != PERMUTED)
			 	&& (mask[j] != FIX)) {
                    mask[j] = PERMUTED;
                    found = 1;
                }
            }
        }
    }

    /* count permutations */
    for (i = 0; i < NUM_PEGS; i++) {
        if (mask[i] == PERMUTED) {
            matched++;
        }
    }

    return matched;
}

void make_5_peg_guesses(stack_t *s)
{
    int i, j, k, l, m;
    struct stack_node *n;

    for (i = 0; i < NUM_COLORS; i++) {
        for (j = 0; j < NUM_COLORS; j++) {
            for(k = 0; k < NUM_COLORS; k++){
                for (l = 0; l < NUM_COLORS; l++) {
                    for (m = 0; m < NUM_COLORS; m++) {
                        n = new_node();
                        n->data = (int *) malloc(NUM_PEGS * sizeof(int));
                        ((int*)(*n).data)[0] = i;
                        ((int*)(*n).data)[1] = j;
                        ((int*)(*n).data)[2] = k;
                        ((int*)(*n).data)[3] = l;
                        ((int*)(*n).data)[4] = m;
                        push(s, n);
                    }
                }
            }
        }
    }
}

void eliminate_guesses(stack_t *s, const int *guess, int red, int white)
{
    int i;
    stack_t tmp;
    struct stack_node *n;
    int num_guesses = s->size;

    init_stack(&tmp);

    for (i = 0; i < num_guesses; i++) {
        n = pop(s);
        if ((count_fix_points(guess, ((int *)(*n).data)) == red)
            && (count_permuted_points(guess, ((int *)(*n).data)) == white)) {
            /* save guess */
            push(&tmp, n);
        } else {
            /* drop guess */
            free(n->data);
            free(n);
        }
    }

    s->head = tmp.head;
    s->size = tmp.size;
}
