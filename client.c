/**
 * @file client.c
 * @author Jannik Vierling 1226434
 * @brief This file implements a mastermind client Programm which connects
 * to a server and sends guesses automatically.
 * @date 8th November 2013
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdarg.h>
#include <limits.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>

#include "mastermind.h"
#include "stack.h"

/** @brief Mask for the key-pegs in the encoded answer */
#define CMASK (0x7)
/** @brief Shift for the mask of the white key-pegs in the encoded answer */
#define WHITE_0FF (3)
/** @brief Shift for the mask of the red key-pegs in the encoded answer */
#define RED_OFF (0)

/** @brief Mask for the parity bit in the encoded answer */
#define MASK_PARITY (0x40)
/** @brief Mask for the max-rounds bit in the encoded answer */
#define MASK_MAX_ROUNDS (0x80)

/** @brief Symbolic name for the parity error stateflag */
#define S_PARITY_ERROR (0x2)
/** @brief Symbolic name for the max-rounds stateflag */ 
#define S_MAX_ROUNDS (0x1)
/** @brief Symbolic name for the normal state */
#define S_NORMAL (0)

#define EXIT_PARITY_ERROR (2)
#define EXIT_GAME_LOST (3)
#define EXIT_MULTIPLE_ERRORS (4)

/** @brief The first guess made by the client */
int first_guess[NUM_PEGS] = {darkblue,darkblue,green,orange,red};

/** @brief The program's name */
static char *progname = "client";

/** @brief The connection file descriptor */
static int connfd;

/** @struct options Contains options parsed from command line */
struct options {
    /** @var options::sail
	*  Member sail Contains a list of address informations about the server
	*/
    struct addrinfo *sail;
} options;

stack_t guess_stack = {.head = NULL, .size = 0};

/** 
 * @brief Prints a user defined error message as well as an error message 
 * depending on errno. Frees resources and terminates the program with return
 * code retval.
 * 
 * @param retval Return code
 * @param fmt Format string used to format the error message
 * @param ... The arguments that are used for the error message
 * @return void
 *
 * @details if fmt is NULL no user defined error message will be printed.
 * All messages are printed to stderr. 
 */
void bail_out(int retval, const char *fmt, ...);

/** 
 * @brief Reads command line arguments and saves them to options
 *
 * @param argc The number of command line arguments
 * @param argv Pointer to the command line argument Array 
 * @param options Pointer to the structure where the options have to be saved 
 * @return void
 */
void parse_args(int argc, char **argv, struct options *options);

/**
 * @brief Tries to connect to server with the arguments specified in options
 * @param options The options that are used to connect to the server
 * @return void
 *
 * @details if the connection could be established the resulting file
 * descriptor is saved in the global variable connfd
 */ 
void connect_to_server(struct options *options);

/**
 * @brief Frees the allocated ressources
 * @return void
 */
void free_resources(void);

/** 
 * @brief Sends the guess to the server
 * @param guess The guess, encoded according to the client/server protocol
 * @return void
 *
 * @details This function returns only if the two bytes of guess are sent
 * @details This function uses connfd
 */
void send_guess(uint16_t guess);

/**
 * @brief Recieves a one byte answer from the server
 * @return Returns the answer encoded according to the client/server protocol
 * 
 * @details This function uses the global variable connfd
 */
uint8_t recv_answer(void);

/** 
 * @brief Encodes a guess according to the client/server protocol
 * @param colors An array of 5 elements wich represents a guess
 * @return The guess encoded according to the client/server protocol
 */
uint16_t build_guess(int colors[]);

/**
 * @brief Parses the encoded answer
 * @param answer The encoded answer
 * @param number_white Pointer to an interger used to store the number of
 * white key-pegs
 * @param number_red Pointer to an integer used to store the number of red
 * key-pegs
 * @return The status of the game
 */
int parse_answer(int answer, int *number_white, int *number_red);

/**
 * @brief Playes mastermind with a server over the connection connfd
 * @param connfd The connection to use for the communikation with the server
 * @param rounds Pointer to an integer in which the number of played rounds
 * is saved
 * @return The state of the game
 *
 * @details This function returns as soon as the state changes
 */
int play(int connfd, int *rounds);

int main(int argc, char **argv)
{
    int rounds = 0;
    int status = S_NORMAL;
    int retval = EXIT_SUCCESS;

    parse_args(argc, argv, &options);
    connect_to_server(&options);

    /* init stack module */
    stack_set_error_handler(&bail_out);

    status = play(connfd,   &rounds);

    if (status == S_NORMAL) {
        printf("%d\n", rounds);
    } else if (status | S_PARITY_ERROR && status | S_MAX_ROUNDS) {
        (void) fprintf(stderr, "Game lost\n");
        (void) fprintf(stderr, "Parity error\n");
        retval = EXIT_MULTIPLE_ERRORS;
    } else if (status | S_PARITY_ERROR) {
        (void) fprintf(stderr, "Parity error\n");
        retval = EXIT_PARITY_ERROR;
    } else if (status | S_MAX_ROUNDS) {
        (void) fprintf(stderr, "Game lost\n");
        retval = EXIT_GAME_LOST;
    }

    free_resources();
    return retval;
}

int play(int connfd, int *rounds)
{
    int answer = 0;
    int number_red = 0;
    int number_white = 0;
    int guess[NUM_PEGS] = {0};
    int status = S_NORMAL;

    init_stack(&guess_stack);
    make_5_peg_guesses(&guess_stack);
    *rounds = 0;

    memcpy(guess, first_guess, NUM_PEGS * sizeof(int));

    while (status == S_NORMAL && number_red != 5) {
        send_guess(build_guess(guess));
        answer = recv_answer();
        parse_answer(answer, &number_white, &number_red);
        eliminate_guesses(&guess_stack, guess, number_red, number_white);
        memcpy(guess, peek(&guess_stack)->data, NUM_PEGS * sizeof(int));
        (*rounds)++;
    }

    return status;
}

uint16_t build_guess(int colors[])
{
    int parity = 0;
    uint16_t guess = 0;
    int i;

    /* insert colors */
    for (i = 0; i < 5; i++) {
        guess |= colors[i] << (3*i);
    }

    /* compute parity */
    for (i = 0; i < 15; i++) {
        parity ^= guess >> i;
    }
    /* set parity bit */
    guess |= parity << 15;

    return guess;
}

int parse_answer(int answer, int *number_white, int *number_red)
{
    int status = S_NORMAL;

    *number_white = answer & (CMASK << WHITE_0FF);
    *number_white >>= WHITE_0FF;

    *number_red = answer & (CMASK << RED_OFF);
    *number_red >>= RED_OFF;

    if (answer & MASK_PARITY) {
        status |= S_PARITY_ERROR;
    }
    if (answer & MASK_MAX_ROUNDS) {
        status |= S_MAX_ROUNDS;
    }

    return status;
}

void send_guess(uint16_t guess)
{
	uint16_t bytes_to_send = sizeof(guess);
	uint8_t *buffer = (uint8_t *)&guess;
	int16_t bytes_sent = 0;
	
	/* loop, as all bytes might not be sent at the same time */
	while (bytes_to_send > 0) {
    	bytes_sent = send(connfd, buffer, bytes_to_send,0);
		if (bytes_sent == -1) {
			bail_out(EXIT_FAILURE, "sending guess");
		}
		bytes_to_send -= bytes_sent;
		buffer += bytes_sent;
	}
}

uint8_t recv_answer(void)
{
    uint8_t answer;
    if (recv(connfd, &answer, sizeof(answer), 0) == -1) {
        bail_out(EXIT_FAILURE, "recieving answer");
    }
    return answer;
}

void connect_to_server(struct options *options)
{
    struct addrinfo *sai = options->sail;

    /* open socket */
    if ((connfd = socket(sai->ai_family, sai->ai_socktype, sai->ai_protocol)) == -1) {
        bail_out(EXIT_FAILURE, "opening socket");
    }

    /* connect to server */
    if (connect(connfd, sai->ai_addr, sai->ai_addrlen) == -1) {
        bail_out(EXIT_FAILURE, "connecting to server");
    }
}

void free_resources()
{
    free_stack(&guess_stack);

    if (connfd >= 0) {
        (void) close(connfd);
    }
    if (options.sail != NULL) {
        (void) freeaddrinfo(options.sail);
    }
}

void parse_args(int argc, char **argv, struct options *options)
{
    struct addrinfo hints = {0};
    int r;

    if (argc > 1) {
        progname = argv[0];
    }
    if (argc != 3) {
        bail_out(EXIT_FAILURE, "%s <server-hostname*> <server-port>", progname);
    }
    /* resolve servername */
    r = getaddrinfo(argv[1], argv[2], &hints, &(options->sail));
    if (r != 0) {
        bail_out(EXIT_FAILURE, "resolving server name: %s", gai_strerror(r));
    }
}

void bail_out(int retval, const char *fmt, ...)
{
    va_list args;

    (void) fprintf(stderr, "%s: ", progname);

    if (fmt != NULL) {
        va_start(args,fmt);
        (void) vfprintf(stderr, fmt, args);
        va_end(args);
    }
    if (errno != 0) {
        (void) fprintf(stderr, ": %s", strerror(errno));
    }
    (void) fprintf(stderr, "\n");

    free_resources();

    exit(retval);
}


